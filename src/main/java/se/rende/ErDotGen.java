package se.rende;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

class ErDotGen {
    static class Schema {
        List<Table> tables = new ArrayList<>();
        List<ForeignKeyLink> foreignKeyLinks = new ArrayList<>();

        Schema(Connection connection) throws SQLException {
            DatabaseMetaData databaseMetaData = connection.getMetaData();

            try (ResultSet tablesResultSet = databaseMetaData.getTables(null, null, null, new String[]{"TABLE"})) {
                while (tablesResultSet.next()) {
                    Table table = new Table(databaseMetaData, tablesResultSet);
                    tables.add(table);

                    try (ResultSet exportedKeys = databaseMetaData.getExportedKeys(null, null, table.name)) {
                        while (exportedKeys.next()) {
                            ForeignKeyLink foreignKeyLink = new ForeignKeyLink(exportedKeys);
                            foreignKeyLinks.add(foreignKeyLink);
                        }
                    }
                }
            }
        }
    }

    static class ForeignKeyLink {
        String toColumnName;
        String fromTableName;
        String fromColumnName;
        String toTableName;

        ForeignKeyLink(ResultSet exportedKeys) throws SQLException {
            toColumnName = exportedKeys.getString("PKCOLUMN_NAME");
            toTableName = exportedKeys.getString("PKTABLE_NAME");
            fromTableName = exportedKeys.getString("FKTABLE_NAME");
            fromColumnName = exportedKeys.getString("FKCOLUMN_NAME");
        }
    }

    static class Table {
        String name;
        List<Column> columns = new ArrayList<>();

        public Table(DatabaseMetaData databaseMetaData, ResultSet tablesResultSet) throws SQLException {
            name = tablesResultSet.getString("TABLE_NAME");

            // get table keys
            HashSet<String> primaryKeyColumns = new HashSet<>();
            ResultSet primaryKeys = databaseMetaData.getPrimaryKeys(null, null, name);
            while (primaryKeys.next()) {
                primaryKeyColumns.add(primaryKeys.getString("COLUMN_NAME"));
            }

            ResultSet columnsResultSet = databaseMetaData.getColumns(null, null, name, null);
            while (columnsResultSet.next()) {
                Column column = new Column(columnsResultSet, primaryKeyColumns.contains(columnsResultSet.getString("COLUMN_NAME")));
                columns.add(column);
            }
        }
    }

    static class Column {
        String name;
        int dataType;
        String typeName;
        int size;
        int decimaldigits;
        String isNullable;
        String is_autoIncrment;
        boolean isPrimaryKey;

        Column(ResultSet columns, boolean isPrimaryKey) throws SQLException {
            name = columns.getString("COLUMN_NAME");
            dataType = columns.getInt("DATA_TYPE");
            typeName = columns.getString("TYPE_NAME");
            size = columns.getInt("COLUMN_SIZE");
            decimaldigits = columns.getInt("DECIMAL_DIGITS");
            isNullable = columns.getString("IS_NULLABLE");
            is_autoIncrment = columns.getString("IS_AUTOINCREMENT");
            this.isPrimaryKey = isPrimaryKey;
        }

        String getTypeDescription() {
            if (dataType == Types.VARCHAR || dataType == Types.CHAR) {
                return typeName + "(" + size + ")";
            } else if (dataType == Types.BIGINT) {
                return typeName;
            }
            return typeName;
        }
    }

    public static void main(String args[]) throws SQLException {
        if (args.length < 3) {
            System.err.println("usage: java -jar xxxx.jar [--dot <dotfilename> | --html <htmlfilename> ] <jdbc-url> <user> <password>");
            System.exit(1);
        }
        String dotFilename = null;
        String htmlFilename = null;
        int i = 0;
        while (i < args.length && args[i].startsWith("--")) {
            String option = args[i++];
            if (option.equals("--dot")) {
                dotFilename = args[i++];
            } else if (option.equals("--html")) {
                htmlFilename = args[i++];
            }
        }
        String jdbcUrl = args[i++];
        String userId = args[i++];
        String password = args[i++];

        try (Connection connection = DriverManager.getConnection(
                jdbcUrl, userId, password);
        ) {
            Schema schema = new Schema(connection);

            if (htmlFilename != null) {
                writeHtml(schema, htmlFilename);
            } else if (dotFilename != null) {
                writeDot(schema, dotFilename);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Write a dot file to stdout.
     * @param schema the data for the dot
     */
    private static void writeDot(Schema schema, String filePath) throws IOException {
        PrintStream pw;
        if (filePath == null || filePath.equals("-")) {
            pw = System.out;
        } else {
            pw = new PrintStream(new FileOutputStream(filePath));
        }
        pw.println("digraph {\n" +
                "  graph [pad=\"0.5\", nodesep=\"0.5\", ranksep=\"2\"];\n" +
                "  node [shape=plain]\n" +
                "  rankdir=LR;\n");

        for (Table table : schema.tables) {
            if (!table.name.startsWith("DATA")) {
                pw.println("  " + table.name + " [label=<\n" +
                        "    <table border=\"0\" cellborder=\"1\" cellspacing=\"0\">\n" +
                        "      <tr><td><b>" + table.name + "</b></td></tr>");

                for (Column column : table.columns) {
                    if (column.isPrimaryKey) {
                        pw.println("      <tr><td align=\"left\" port=\"" + column.name + "\"><u>" + column.name + " " + column.getTypeDescription() + "</u></td></tr>");
                    } else {
                        pw.println("      <tr><td align=\"left\" port=\"" + column.name + "\">" + column.name + " " + column.getTypeDescription() + "</td></tr>");
                    }
                }
                pw.println("    </table>>];");
            }
        }

        for (ForeignKeyLink foreignKeyLink: schema.foreignKeyLinks) {
            pw.println("  " + foreignKeyLink.fromTableName + ":" + foreignKeyLink.fromColumnName + " -> " + foreignKeyLink.toTableName + ":" + foreignKeyLink.toColumnName + ";");
        }
        pw.println("}");
    }
    /**
     * Write a dot file to stdout.
     * @param schema the data for the dot
     */
    private static void writeHtml(Schema schema, String filePath) throws IOException {
        PrintStream pw;
        if (filePath == null || filePath.equals("-")) {
            pw = System.out;
        } else {
            pw = new PrintStream(new FileOutputStream(filePath));
        }
        pw.println("<html><head><style>");
        pw.println("td {border: 1px solid black;");
        pw.println("</style></head><body>");

        for (Table table : schema.tables) {
            if (!table.name.startsWith("DATA")) {
                pw.println("  <p><table border=\"0\" cellborder=\"1\" cellspacing=\"0\">\n" +
                        "      <tr><td><b>" + table.name + "</b></td></tr>");

                for (Column column : table.columns) {
                    String s = column.name + " " + column.getTypeDescription();
                    if (column.isPrimaryKey) {
                        s = "<u>" + s + "</u>";
                    }
                    pw.println("      <tr><td align=\"left\">" + s + "</td><td></td></tr>");
                }
                pw.println("    </table></p>");
            }
        }

//        for (ForeignKeyLink foreignKeyLink: schema.foreignKeyLinks) {
//            pw.println("  " + foreignKeyLink.fromTableName + ":" + foreignKeyLink.fromColumnName + " -> " + foreignKeyLink.toTableName + ":" + foreignKeyLink.toColumnName + ";");
//        }
        pw.println("</body></html>");
    }
}
